import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyGame {

    public static final String REGEX_SPACE = "\\s+";
    public static final String CALCULATE_ERROR = "Calculate Error";

    public String calculateWordGameResult(String words) {
        try {
            String[] wordsArray = words.split(REGEX_SPACE);

            List<Word> wordList = Arrays.stream(wordsArray)
                    .collect(Collectors.groupingBy(word -> word, Collectors.counting()))
                    .entrySet()
                    .stream()
                    .map(entry -> new Word(entry.getKey(), entry.getValue().intValue()))
                    .sorted((word1, word2) -> (word2.getWordCount() - word1.getWordCount()))
                    .collect(Collectors.toList());

            return outputResult(wordList);

        } catch (Exception e) {
            return CALCULATE_ERROR;
        }
    }

    public String outputResult(List<Word> wordList) {
        return wordList.stream()
                .map(word -> word.getValue() + " " + word.getWordCount())
                .collect(Collectors.joining("\n"));
    }
}
