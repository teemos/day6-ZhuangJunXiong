import java.util.Objects;

public class Word {
    private String value;
    private int count;

    public Word(String value, int couont) {
        this.value = value;
        this.count = couont;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Word word = (Word) o;
        return Objects.equals(value, word.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    public String getValue() {
        return this.value;
    }

    public int getWordCount() {
        return this.count;
    }

    public int addWordCount() {
        return this.count += 1;
    }

}
